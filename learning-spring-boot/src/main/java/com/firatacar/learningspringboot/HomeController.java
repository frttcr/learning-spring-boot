package com.firatacar.learningspringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @date 20.07.2019 - 02:30
 * @author fRttcr
 */
@RestController
public class HomeController {
	
	/**
	 * @param name for greeting message
	 * @return greeting message
	 */
	@GetMapping		
	public String greeting(@RequestParam(required = false,
			defaultValue = "") String name) {
		return name.equals("") ? "Hey!" : "Hey, " + name + "!";
	}
	
}
