package com.firatacar.learningspringboot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.publisher.Flux;

/**
 * @date 20.07.2019 - 02:44
 * @author fRttcr
 */
@Configuration		// @Configuration marks this class as a source of beans
public class LoadDatabase {
	
	@Bean
	/**
	 * Spring Boot runs all CommandLineRunner beans after the entire application is up and running. 
	 * This bean definition requests a copy of ChapterRepository.
	 * 
	 * @param repository instance of ChapterRepository
	 * @return set of Chapter data
	 */
	CommandLineRunner init(ChapterRepository repository) {
		return args -> {
			Flux.just(
					new Chapter("Quick Start with Java"),
					new Chapter("Reactive Web with Spring Boot"),
					new Chapter("... and more!"))
					.flatMap(repository::save)
					.subscribe(System.out::println);
		};
	}
	
}
