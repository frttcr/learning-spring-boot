package com.firatacar.learningspringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;

/**
 * @date 20.07.2019 - 02:54
 * @author fRttcr
 */
@RestController
public class ChapterController {

	private final ChapterRepository repository;
	
	/**
	 * Constructor injection is used to automatically load it with a copy of ChapterRepository. 
	 * With Spring, if there is only one constructor call, 
	 * there is no need to include an @Autowired annotation.
	 * 
	 * @param repository
	 */
	public ChapterController(ChapterRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/chapters")
	public Flux<Chapter> listing() {
		return repository.findAll();
	}
	
}
