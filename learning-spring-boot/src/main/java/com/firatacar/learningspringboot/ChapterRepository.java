package com.firatacar.learningspringboot;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @date 20.07.2019 - 02:38
 * @author fRttcr
 */
public interface ChapterRepository extends ReactiveCrudRepository<Chapter, String> {

}
