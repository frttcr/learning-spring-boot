package com.firatacar.learningspringboot;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @date 20.07.2019 - 02:35
 * @author fRttcr
 */
@Data 
@Document
public class Chapter {
	
	@Id
	private String id;
	private String name;
	
	/**
	 * @param name for the name of the book
	 */
	public Chapter(String name) {
		this.name = name;
	}
	
}
